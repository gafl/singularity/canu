# canu Singularity container
Bionformatics package canu<br>
Canu is a fork of the Celera Assembler designed for high-noise single-molecule sequencing.
http://canu.readthedocs.org/
canu Version: 1.9<br>

Singularity container based on the recipe: Singularity.canu_v1.9

Package installation using Miniconda3 V4.7.12<br>

image singularity (V>=3.3) is automaticly built (gitlab-ci) and pushed on the registry using the .gitlab-ci.yml <br>

can be pull (singularity version >=3.3) with:<br>
singularity pull canu_v1.9.sif oras://registry.forgemia.inra.fr/gafl/singularity/canu/canu:latest


